var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {
    title1: 'Mr. Dagler\'s Homepage',
    title2: 'my webpage',
  });
});

/* GET math sl page. */
router.get('/mathsl', function(req, res, next) {
  res.render('mathsl', {
    title1: 'Math SL Year 1',
    title2: 'our Math SL',
   });
});

/* GET computer science page. */
router.get('/computer_science', function(req, res, next) {
  res.render('computer_science', {
    title1: 'Intro to Computer Science',
    title2: 'our Computer Science',
   });
});

/* GET computer science page. */
router.get('/robotics', function(req, res, next) {
  res.render('robotics', {
    title1: 'Computer Science and Robotics',
    title2: 'our Robotics',
   });
});

module.exports = router;
